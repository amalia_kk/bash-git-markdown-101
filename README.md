# this is a title

this is a paragraph.

## this is a smaller title.

hello these are words

### this is an even smaller title.

Underneath here I will have a list:

- Bullet point 1
- Bullet 2
- Etc

Now I am going to make an ordered list:

1. point one 
2. point two

### special sections for code

This is how you show code on a markdown.

The first example is a code block:

``` bash
# This is a comment in code
# Changing directory in bash

cd <folder>
```

This is how you make a coded line `git status`

**This is bold type**

__This is also bold type__

*This is text in italics*

<u>This is how you underline text</u>

:)


bash commands:

cd = change directory 
pwd = print current working directory
mkdir = create a new folder
ls = list short
touch = create file
mv = move or rename


How to add a remote: 

1. Create a new repository on bitbucket
2. Once created, look at step 2, copy line 1 of code and paste it into your terminal
3. Next type ```git remote --v``` and there should be two lines displayed: a push and a fetch
4. Then type ```git push origin master```, type 'yes' if required and then refresh your repo to ensure it has worked.
5. Now go to your terminal and cd to where you want to be
6. Create any new files  such as README.md
7. GO to your vs code, open the folder you've created, add some text to your 
8. Use ```ls -a``` to check the files you have created
9. In the terminal, type 'git init'
10. 